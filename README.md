##Segue algumas instruções a respeito da Utilização da API:##


Os Metodos Get, Post e Delete estão referenciados na Controller PontoController.
Por padrão os mesmos podem ser acessados pelo seguinte endereço: "Localhost:porta/Api/Ponto"

Para os métodos, foi implementada uma model "Ponto", que possui os seguintes parametros:
	Nome 	- Nome do usuário
	Setor 	- Setor do usuário
	Data	- Data da inserção das batidas de ponto
	Hr1		- Horário de entrada
	Hr2		- Horário de saida Almoço
	Hr3		- Horário de retorno do Almoço
	Hr4		- Encerramento do Expediente
	
Desta forma, podemos realizar a request Post(nome,setor,data,hr1,hr2,hr3,hr4)gerando a seguinte url:
	[Post]Localhost:porta/Api/Ponto/?nome=""&setor=""&data=""%hr1=""hr2=""hr3=""hr4=""
	Este irá gerar adicionar um objeto do tipo Ponto à lista com o sparametros informados (a princípio todos nomodo string);
	
A request Get irá retornar uma lista contendo todos os objetos do tipo Ponto gerados através do Post()
	[Get]Localhost:porta/Api/Ponto/
	
A requisição Delete, por sua vez irá deletar um item desta lista tendo como parametro o nome
	[Delete]Localhost:porta/Api/Ponto/?nome=""
	



Duvidas à disposição;
Rafael Gonzaga
rafael.gonaga@hotmail.com.br
