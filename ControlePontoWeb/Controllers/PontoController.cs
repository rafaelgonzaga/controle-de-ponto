﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ControlePontoWeb.Models;

namespace ControlePontoWeb.Controllers
{
    public class PontoController : ApiController
    {
        //Criando a Lista de objetos Ponto.
        public static List<Ponto> LPonto = new List<Ponto>();


        //Método Get
        public List<Ponto> Get()
        {
            return LPonto;
        }


        //Método Post
        public void Post(string nome, string setor, string data, string hr1, string hr2, string hr3, string hr4)
        {
            LPonto.Add(new Ponto(nome, setor, data, hr1, hr2, hr3, hr4));
        }


        //Metodo Delete - Irá deletar os registros pelo nome do usuário
        public void Delete(string nome)
        {
            LPonto.RemoveAt(LPonto.IndexOf(LPonto.First(p => p.Nome.Equals(nome))));
        }


        
    }
}
