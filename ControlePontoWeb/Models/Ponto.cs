﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControlePontoWeb.Models
{
    public class Ponto
    {
        //Variaves de dados a serem salvos
        public string Nome { get; set; }
        public string Setor { get; set; }
        public string Data { get; set; }
        public string Hr1 { get; set; }
        public string Hr2 { get; set; }
        public string Hr3 { get; set; }
        public string Hr4 { get; set; }

            public Ponto(string nome, string setor, string data, string hr1, string hr2, string hr3, string hr4)
        {
            this.Nome = nome;
            this.Setor = setor;
            this.Data = data;
            this.Hr1 = hr1;
            this.Hr2 = hr2;
            this.Hr3 = hr3;
            this.Hr4 = hr4;
        }
     
    }
}